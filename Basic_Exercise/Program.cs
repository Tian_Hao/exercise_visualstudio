﻿using System;

namespace Basic_Exercise
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            //Exercise1 text
            /*Console.WriteLine("Exercise 1 :");
            Console.WriteLine("Hello World: Tian Hao");*/

            //Exercise2 multiplcation
            /*int firstNumber = 1;
            int secondNumber = 2;
            int total = firstNumber + secondNumber;
            Console.WriteLine(total);*/
                
            //Exercise3 division
            /*int firstNumber = 3;
            int secondNumber = 6;
            int division = secondNumber / firstNumber;
            Console.WriteLine(division);*/

            //Exercise7 Math
            int firstNumber = 25;
            int secondNumber = 4;
            int add = firstNumber + secondNumber;
            int sub = firstNumber - secondNumber;
            int multi = firstNumber * secondNumber;
            int div = firstNumber / secondNumber;
            Console.WriteLine( firstNumber+" + "+secondNumber+" = "+add);
            Console.WriteLine( firstNumber+" - "+secondNumber+" = "+sub);
            Console.WriteLine( firstNumber+" * "+secondNumber+" = "+multi);
            Console.WriteLine( firstNumber+" / "+secondNumber+" = "+div);

        }
    }
}
